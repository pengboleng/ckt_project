# coding:utf-8
import xlrd# 需要安装xdrd模块：pip install xlrd
from datetime import datetime
from xlrd import xldate_as_tuple

class ExcelUtil():
    def __init__(self, excelPath, sheetName):
        self.data = xlrd.open_workbook(excelPath)
        self.sheet = self.data.sheet_by_name(sheetName)
        self.keys = self.sheet.row_values(0)
        self.rowNum = self.sheet.nrows
        self.colNum = self.sheet.ncols
    def list_data(self):
        if self.rowNum <= 1:
            print("the sum of line is less than 1")
        else:
            res = []
            j=1
            for rowx in range(1,self.rowNum):
                row_data = []
                for colx in range(self.colNum):
                    cell_type = self.sheet.cell(rowx, colx).ctype
                    cell_value = self.sheet.cell_value(rowx, colx)
                    # data type 0 --empty,1 --string, 2 --number(all are float), 3 --date, 4 --boolean, 5 --error
                    if cell_type == 2 and cell_value % 1 == 0.0:# ctype is 2 and it is float
                        cell_value = str(int(cell_value))
                    elif cell_type == 3:
                        date = datetime(*xldate_as_tuple(cell_value, 0))
                        cell_value = date.strftime('%Y/%m/%d %H:%M:%S')
                    elif cell_type == 4:
                        cell_value = True if cell_value == 1 else False
                    row_data.append(cell_value)
                res.append(row_data)
                j+=1
            return res
    def dict_data(self):
        if self.rowNum <= 1:
            print("the sum of line is less than 1")
        else:
            r = []
            j=1
            for rowx in range(1,self.rowNum):
                s = {}
                for colx in range(self.colNum):
                    cell_type = self.sheet.cell(rowx, colx).ctype
                    cell_value = self.sheet.cell_value(rowx, colx)
                    # data type 0 --empty,1 --string, 2 --number(all are float), 3 --date, 4 --boolean, 5 --error
                    if cell_type == 2 and cell_value % 1 == 0.0:# ctype is 2 and it is float
                        cell_value = str(int(cell_value))
                    elif cell_type == 3:
                        date = datetime(*xldate_as_tuple(cell_value, 0))
                        cell_value = date.strftime('%Y/%m/%d %H:%M:%S')
                    elif cell_type == 4:
                        cell_value = True if cell_value == 1 else False
                    s[self.keys[colx]] = cell_value
                r.append(s)
                j+=1
            return r
if __name__ == "__main__":
    filepath = "../data/test1.xlsx"
    sheetName = "Sheet1"
    excelUtil = ExcelUtil(filepath, sheetName)
    print(excelUtil.list_data())
    print(excelUtil.dict_data())
