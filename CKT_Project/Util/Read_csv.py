import csv

class CSVUtil():
    def __init__(self, filepath):
        self.filepath = filepath

    def list_data(self):
        # 读取CSV文件
        value_rows = []
        with open(self.filepath, "r") as f:
            f_csv = csv.reader(f)
            next(f_csv)  # 如果从第2个开始读取数据，则加入该行代码
            for r in f_csv:
                value_rows.append(r)
        return value_rows

if __name__=="__main__":
    filepath="../TeatData/Testdata.csv"
    u=CSVUtil(filepath)
    print(u.list_data())