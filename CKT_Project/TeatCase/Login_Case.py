import unittest
import pytest
from ddt import ddt,data,unpack
from Page.LoginPage import LoginPage
from Util.Read_csv import CSVUtil
from Base.MyBase import MyBase
from time import sleep
from Base.logs import Logger

filePath="../TeatData/前台登录.csv"
file=CSVUtil(filePath)
testdata=file.list_data()

logger = Logger(logger='登录功能测试').getlog()

# 前台登录
@ddt
class TheHeaderTestCase(MyBase):
    @data(*testdata)
    @unpack
    def test_login(self,username,password,exceptd_result):
        lp = LoginPage(self.driver)
        lp.open()
        lp.login(username,password)
        if exceptd_result=="1":
            sleep(1)
            url = self.driver.current_url
            self.assertEqual(url,'https://www.chuangkit.com/designtools/designindex')
            # try:
            #     logger.info("截图成功")
            # except Exception as e:
            #     logger.error('截图失败')

if __name__ == '__main__':
    unittest.main()



