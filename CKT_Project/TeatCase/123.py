import unittest
from ddt import ddt,data,unpack
from Util.Read_csv import CSVUtil
from Base.MyBase import MyBase
from time import sleep
from Page.LoginPage import LoginPage
# from Base.logs import Logger

# logger = Logger(logger='登录功能测试').getlog()

filePath="../TeatData/前台登录.csv"
file=CSVUtil(filePath)
testdata=file.list_data()
print(testdata)

# 前台登录
@ddt
class TheHeaderTestCase(MyBase):
    @data(*testdata)
    @unpack
    def test_login(self,username,password,exceptd_result):
        lp = LoginPage(self.driver)
        lp.open()
        lp.login(username,password)
        if exceptd_result=="1":
            try:
                self.assertEqual(self.driver.current_url,"https://www.chuangkit.com/designtools/designindex")
                self.logger("个人免费账号-登录成功")
            except Exception as e:
                self.logger("个人免费账号-登录失败")
                self.screenshot()
        elif exceptd_result=="2":
            try:
                self.assertEqual(self.driver.current_url, "https://www.chuangkit.com/designer/create")
                self.logger("设计师账号-登录成功")
            except Exception as e:
                self.logger("设计师账号-登录失败")
                self.screenshot()


if __name__ == '__main__':
    unittest.main()

