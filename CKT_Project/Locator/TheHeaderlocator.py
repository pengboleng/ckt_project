from selenium.webdriver.common.by import By

class TheHeadeLocator:
    def __init__(self):
        #未登录时，页面顶部的按钮定位
        self.dl=(By.LINK_TEXT,"登录")  # 顶部“登录”按钮定位
        self.zc=(By.LINK_TEXT,"注册")  # 顶部“注册”按钮定位
        self.sy=(By.LINK_TEXT, "首页")  # 顶部“首页”按钮定位
        self.mbzx=(By.LINK_TEXT, "模板中心")  # 顶部“模板中心”按钮定位
        self.grvip=(By.LINK_TEXT, "个人VIP")  # 顶部“个人VIP”按钮定位
        self.qyvip=(By.LINK_TEXT, "企业VIP")  # 顶部“企业VIP”按钮定位
        self.khd=(By.LINK_TEXT, "客户端")  # 顶部“客户端”按钮定位
        self.cyfw=(By.LINK_TEXT, "创意服务")  # 顶部“创意服务”按钮定位
        self.gnjs=(By.LINK_TEXT, "功能介绍")  # 顶部“功能介绍”按钮定位
        self.gd=(By.XPATH, "//i[contains(@class,'arrow-dropdown lt-icon')]")  # 顶部“更多”按钮定位
        self.apijr=(By.LINK_TEXT, "API接入")  # 顶部“Api接入”按钮定位
        self.dzsj=(By.LINK_TEXT, "定制设计")  # 顶部“定制设计”按钮定位
        self.qywx=(By.LINK_TEXT,"企业微信")  # 顶部“企业微信”按钮定位
        #登录后，页面顶部的按钮定位
        self.xxtz=(By.XPATH,"//i[contains(@class,'icon-font lt-icon')]") # “消息通知”按钮定位
        self.zhzx=(By.ID,"GUIDE_3_USER") # “账号中心”按钮定位

