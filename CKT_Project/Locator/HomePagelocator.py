from selenium.webdriver.common.by import By  # 导入定位类，By类
class HomePageLocator:
    def __init__(self,):
        #左侧按钮定位
        self.tj=(By.XPATH,"//span[text()='推荐']")  #首页左侧"推荐"按钮定位
        self.wdsj=(By.XPATH,"(//span[text()='我的设计'])[1]")  #首页左侧"我的设计"按钮定位
        self.cjsj=(By.XPATH,"//span[text()='创建设计']")  #首页左侧"创建设计"按钮定位
        self.yjkt=(By.XPATH,"//span[text()='一键抠图']")  #首页左侧"一键抠图"按钮定位
        self.pmsj=(By.XPATH,"//span[text()='平面设计']")  #首页左侧"平面设计"按钮定位
        self.mbzx=(By.XPATH,"(//span[text()='模板中心'])[1]")  #首页左侧"平面设计"里"模板中心"按钮定位
        self.tpbj=(By.XPATH,"//span[text()='图片编辑']")  #首页左侧"平面设计"里"图片编辑"按钮定位
        self.gif_dt=(By.XPATH,"//span[text()='GIF动图']")  #首页左侧"平面设计’里"GIF动图"按钮定位
        self.spsj=(By.XPATH,"//span[text()='视频制作']")  #首页左侧"视频制作"按钮定位
        self.mbzx=(By.XPATH,"(//span[text()='模板中心'])[2]")  #首页左侧"视频制作"里"模板中心"按钮定位
        self.spjj=(By.XPATH,"//span[text()='视频剪辑']")  #首页左侧"视频制作"里"视频剪辑"按钮定位
        #中间按钮定位
        self.ss_input=(By.ID,"search")  #首页"搜索框"定位
        self.ss_button=(By.XPATH,"//div[@class='search-icon']//span[1]")  #首页"搜索"按钮定位