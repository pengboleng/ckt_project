from selenium.webdriver.common.by import By # 导入定位类，By类

class LoginLocator:
    def __init__(self):
        #页面共有按钮定位
        self.qhdlfs = (By.XPATH, "//button[@title='切换登录方式']") # 登录弹窗“切换登录方式”按钮定位
        self.gb = (By.XPATH,"//i[@class='lt-icon lt-icon-pic-close']") # “关闭”按钮定位
        self.qq = (By.XPATH, "//i[@class='qq']")  # “QQ”按钮定位
        self.wb = (By.XPATH, "//i[@class='weibo']")  # “微博”按钮定位
        self.qywx = (By.CLASS_NAME, "wxwork")  # “企业微信”按钮定位
        self.dd = (By.CLASS_NAME, "dingtalk")  # “钉钉”按钮定位
        self.yhxy = (By.LINK_TEXT, "用户协议")  # “用户协议”按钮定位
        self.yszc = (By.LINK_TEXT, "隐私政策")  # “隐私政策”按钮定位
        self.sjhzc = (By.LINK_TEXT, "手机号注册")  # “手机号注册”按钮定位


        #微信扫码页按钮定位
        self.djzl = (By.XPATH,"//span[text()='点击这里']") # “点击这里”按钮定位

        #账号密码登录页/手机验证码登录页共有按钮
        self.wjmm = (By.XPATH, "//span[text()='忘记密码 ?']")  # "忘记密码"按钮定位
        self.dl_button = (By.XPATH, "//button[@type='button']")  # “登录”按钮定位
        #账号密码登录页按钮定位
        self.sjh_yx_input = (By.XPATH, "//input[@placeholder='邮箱/手机号']")  # “手机号/邮箱"文本框定位
        self.mm_input = (By.XPATH, "//input[@placeholder='密码']")  # “密码"文本框定位
        self.sjyzmdl = (By.XPATH,"//span[text()='手机验证码登录']") # "手机验证码登录"按钮定位
        #手机验证码登录页按钮
        self.sjh_input = (By.XPATH,"//input[@placeholder='请输入手机号']") # “手机号"文本框定位
        self.dxyzm_input = (By.XPATH,"//input[@placeholder='输入短信验证码']") # “短信验证码"文本框定位
        self.hqdxyzm_button = (By.XPATH,"//span[text()='获取短信验证码']") # “获取短信验证码"按钮定位
        self.zhmidl = (By.XPATH,"//span[text()='账号密码登录']") # "账号密码登录"按钮定位

        #找回密码页共有按钮定位
        self.fhdl = (By.XPATH,"//span[text()='返回登录']") # "返回登录"按钮定位
        self.xyb = (By.XPATH,"//span[text()='下一步']") # "下一步"按钮定位
        #手机号找回Tab页按钮定位
        self.sjhzh=(By.XPATH,"//div[text()=' 手机号找回 ']") # “手机号找回”按钮定位
        self.sjh_input = (By.XPATH,"//input[@placeholder='请输入手机号']") # “手机号"文本框定位
        self.dxyzm_input = (By.XPATH,"//input[@placeholder='输入短信验证码']") # “短信验证码"文本框定位
        self.hqdxyzm_button = (By.XPATH, "//span[text()='获取短信验证码']")  # “获取短信验证码"按钮定位
        #邮箱找回Tab页按钮定位
        self.yxzh = (By.XPATH,"//div[text()=' 邮箱找回 ']") # “邮箱找回”按钮定位
        self.yx = (By.CLASS_NAME,"lt-input") # “邮箱”文本框定位






