from selenium.webdriver.common.by import By  # 导入定位类，By类
class TemplateCenterPageLocator:
    def __init__(self,):
        #左侧按钮定位
        self.jx = (By.XPATH,"//span[text()='精选']")
        self.yxhb = (By.XPATH,"//span[text()='营销海报'])[2]")
        self.xmtpt = (By.XPATH, "//span[text()='新媒体配图'])[2]")
        self.yswl = (By.XPATH, "//span[text()='印刷物料']")
        self.spmb = (By.XPATH, "//span[text()='视频模板']")
        self.bgwd = (By.XPATH, "//span[text()='办公文档']")
        self.gxdz = (By.XPATH, "//span[text()='个性定制']")
        self.sjsh = (By.XPATH, "//span[text()='社交生活']")
        self.dssj = (By.XPATH, "//span[text()='电商设计']")
        self.chys = (By.XPATH, "//span[text()='插画元素']")
        self.zbtk = (By.XPATH, "//span[text()='正版图库']")
        self.zbzt = (By.XPATH, "//span[text()='正版字体']")
        self.tmbxq = (By.XPATH, "//span[text()='提模板需求']")
