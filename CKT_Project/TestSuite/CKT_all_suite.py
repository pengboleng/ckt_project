import unittest
from time import strftime
from Util.HTMLTestRunner import HTMLTestRunner

if __name__=="__main__":
    suite=unittest.defaultTestLoader.discover("../TeatCase/","*case.py")
    current_data_time=strftime("%Y%m%d_%H%M%S")
    with open("../TestReport/CKT_test_report_%s.html"%current_data_time,"wb") as html_report_file:
        runner=HTMLTestRunner(stream=html_report_file,title="创客贴测试结果",description="创客贴测试报告")
        runner.run(suite)
