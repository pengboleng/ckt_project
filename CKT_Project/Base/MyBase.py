import unittest
from selenium import webdriver
from Base.logs import Logger
import time
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.support.wait import WebDriverWait

logger = Logger(logger='截图是否成功').getlog()

class MyBase(unittest.TestCase):
    # 通用函数：浏览器启动，隐式等待，创客贴网址
    def setUp(self):
        # --页面隐藏运行
        # chrome_options = webdriver.ChromeOptions()
        # chrome_options.add_argument("--headless")
        # self.driver=webdriver.Chrome(chrome_options=chrome_options)

        # -- 页面显示运行
        self.driver=webdriver.Chrome()
        self.driver.implicitly_wait(15)
        self.driver.maximize_window()
        self.accept_next_alert=True

    # 通用函数：浏览器关闭
    def tearDown(self):
        self.driver.quit()

    # 通用函数：判断元素是否出现
    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    # 通用函数：判断弹窗是否出现
    def is_alert_present(self):
        try:
            self.driver.switch_to.alert
        except NoAlertPresentException as e:
            return False
        return True

    # 通用函数：获得弹出框里的文本后，关闭弹出框
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to.alert
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept() # 点击确定
            else:
                alert.dismiss() # 点击取消
            return alert_text
        finally:
            self.accept_next_alert = True

    # 获取屏幕截图
    def screenshot(self):
        name = time.strftime('%Y%m%d-%H-%M-%S') + '.png'
        path = 'D:\PycharmProjects\CKT_Project\Screen'
        self.driver.get_screenshot_as_file(path + '/' + name)
        try:
            self.driver.get_screenshot_as_file(path + '/' + name)
            logger.info("截图成功")
        except Exception as e:
            logger.error('截图失败')

    #封装显式等待
    def webdriver_wait(self,loc):
        wdw = WebDriverWait(self, 15, 0.5)
        element=self.driver.find_element(loc)
        wdw.until(element)







