from Locator.TheHeaderlocator import TheHeadeLocator
#顶部banner
class TheHeaderPage:
    # 构造函数，用于给该类的属性赋值
    def __init__(self,driver):
        self.driver=driver
        self.Th=TheHeadeLocator()
    def open(self):
        self.driver.get("https://www.chuangkit.com/")
    def login(self):
        self.driver.find_element(*self.Th.dl).click() #点击登录按钮
    def registered(self):
        self.driver.find_element(*self.Th.zc).click() #点击注册按钮
    def home_page(self):
        self.driver.find_element(*self.Th.sy).click() #点击首页按钮
    def template_center(self):
        self.driver.find_element(*self.Th.mbzx).click() #点击模板中心按钮
    def persona_vip(self):
        self.driver.find_element(*self.Th.grvip).click() #点击个人vip按钮
    def enterprise_vip(self):
        self.driver.find_element(*self.Th.qyvip).click() #点击企业vip按钮
    def client(self):
        self.driver.find_element(*self.Th.khd).click() #点击客户端按钮
    def creative_services(self):
        self.driver.find_element(*self.Th.cyfw).click() #点击创意服务按钮
    def function_is_introduced(self):
        self.driver.find_element(*self.Th.gnjs).click() #点击功能介绍按钮
    def more(self):
        self.driver.find_element(*self.Th.gd).click() #点击更多按钮
    def api_access_to_the(self):
        self.driver.find_element(*self.Th.apijr).click() #点击api接入按钮
    def custom_design(self):
        self.driver.find_element(*self.Th.dzsj).click() #点击定制设计按钮
    def Enterprise_WeChat(self):
        self.driver.find_element(*self.Th.qywx).click() #点击定制设计按钮






