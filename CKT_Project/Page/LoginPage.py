from Locator.Loginlocator import LoginLocator # 导入“登录页”按钮定位类
from Locator.TheHeaderlocator import TheHeadeLocator # 导入“顶部”按钮定位类
from time import sleep
class LoginPage:
    # 构造函数，用于给该类的属性赋值
    def __init__(self,driver):
        self.driver=driver
        self.Lg_loc=LoginLocator() # 实例化“登录页”按钮定位
        self.TH_loc=TheHeadeLocator() # 实例化“顶部”按钮定位
    def open(self):
        self.driver.get("https://www.chuangkit.com/") # 封装“打开正式环境网址”方法
    def login(self,username,password): # 封装“登录”方法
        self.driver.find_element(*self.TH_loc.dl).click() #点击顶部“登录”
        self.driver.find_element(*self.Lg_loc.qhdlfs).click() #点击"切换登录方式"按钮
        self.driver.find_element(*self.Lg_loc.sjh_yx_input).send_keys(username) #点击“手机号/邮箱输入框”，并输入用户名
        self.driver.find_element(*self.Lg_loc.mm_input).send_keys(password) #点击“密码输入框”，并输入密码
        self.driver.find_element(*self.Lg_loc.dl_button).click() #点击“登录”按钮


